package PermMissingElem;


/*
A zero-indexed array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
Your goal is to find that missing element.
Write a function:
class Solution { public int solution(int[] A); }
that, given a zero-indexed array A, returns the value of the missing element.
For example, given array A such that:
  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5
the function should return 4, as it is the missing element.
Assume that:
N is an integer within the range [0..100,000];
the elements of A are all distinct;
each element of array A is an integer within the range [1..(N + 1)].
Complexity:
expected worst-case time complexity is O(N);
expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified

*/

// you can also use imports, for example:
// import java.util.*;

// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Solution {
    public int solution(int[] A) {
        // write your code in Java SE 8
        long N = A.length;
        long sum = 0;
        for(int i = 0; i < N; ++i) {
            sum += A[i];
        }

        long missing = (1+(N+1)) * (N+1) /2 - sum;

        return  (int) missing;
    }

    //-----------test------------------------------------------------
    @Test
    public void test() {
        int N = 100001;
        int a = (1+(N+1)) * (N+1) /2;
        System.out.println(a);
    }

    @Test
    public void test1() {
        int[] A = new int[100000];
        for(int i = 0; i < 100000; ++i) {
            A[i] = i+1;
        }
        A[0] = 100001;
        System.out.println(A[100000-1]);
        int expected = 1;

        int actual = solution(A);
        System.out.println(actual);

        assertEquals(expected, actual);
    }
}
