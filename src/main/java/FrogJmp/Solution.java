package FrogJmp;


/*
A small frog wants to get to the other side of the road. The frog is currently located at position X and wants to get to a position greater than or equal to Y. The small frog always jumps a fixed distance, D.
Count the minimal number of jumps that the small frog must perform to reach its target.
Write a function:
class Solution { public int solution(int X, int Y, int D); }
that, given three integers X, Y and D, returns the minimal number of jumps from position X to a position equal to or greater than Y.
For example, given:
  X = 10
  Y = 85
  D = 30
the function should return 3, because the frog will be positioned as follows:
after the first jump, at position 10 + 30 = 40
after the second jump, at position 10 + 30 + 30 = 70
after the third jump, at position 10 + 30 + 30 + 30 = 100
Assume that:
X, Y and D are integers within the range [1..1,000,000,000];
X ≤ Y.
Complexity:
expected worst-case time complexity is O(1);
expected worst-case space complexity is O(1).

*/

// you can also use imports, for example:
// import java.util.*;

// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Solution {
    public int solution(int X, int Y, int D) {
        // write your code in Java SE 8
        int count = (Y-X) / D;
        int left = (Y-X) % D;
        return left == 0 ? count : count+1;
    }

    //-------------------------------------------------

    private Random rdm = new Random();
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    // [from, to)
    private int random(int from, int to) {
        return rdm.nextInt(to - from) + from;
    }

    // A[0]...A[n-1]
    private int random(int[] A) {
        int index = random(0, A.length);
        return A[index];
    }

    @Test
    public void test1() {
        int X = 0, Y = 20, D = 19;
        int expected = 2;

        int actual = solution(X, Y, D);

        assertEquals(expected, actual);
    }

    @Test
    public void test2() {
        int X = 0, Y = 20, D = 20;
        int expected = 1;

        int actual = solution(X, Y, D);

        assertEquals(expected, actual);
    }

    @Test
    public void test3() {
        int X = 0, Y = 20, D = 21;
        int expected = 1;

        int actual = solution(X, Y, D);

        assertEquals(expected, actual);
    }

    @Test
    public void test4() {
        int X = 10, Y = 85, D = 30;
        int expected = 3;

        int actual = solution(X, Y, D);

        assertEquals(expected, actual);
    }

}
