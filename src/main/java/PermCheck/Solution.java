package PermCheck;


/*
A non-empty zero-indexed array A consisting of N integers is given.
A permutation is a sequence containing each element from 1 to N once, and only once.
For example, array A such that:
    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
is a permutation, but array A such that:
    A[0] = 4
    A[1] = 1
    A[2] = 3
is not a permutation, because value 2 is missing.
The goal is to check whether array A is a permutation.
Write a function:
class Solution { public int solution(int[] A); }
that, given a zero-indexed array A, returns 1 if array A is a permutation and 0 if it is not.
For example, given array A such that:
    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
the function should return 1.
Given array A such that:
    A[0] = 4
    A[1] = 1
    A[2] = 3
the function should return 0.
Assume that:
N is an integer within the range [1..100,000];
each element of array A is an integer within the range [1..1,000,000,000].
Complexity:
expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.

*/

// you can also use imports, for example:
// import java.util.*;

// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Solution {
    public int solution(int[] A) {
        // write your code in Java SE 8
        final int N = A.length;
        int[] seen = new int[N + 1];
        for(int i = 0 ;i < N; ++i) {
            if(A[i] > N) return 0; // exceeding the range
            seen[A[i]]++;
        }

        for(int i = 1; i < N+1; ++i) {
            if(seen[i] != 1) return 0; // seen many times
        }

        return 1;
    }

    //-----------------------test------------------------------------

    @Test
    public void test1() {
        int[] A = {4, 1, 3, 2};
        int expected = 1;

        int actual = solution(A);

        assertEquals(expected, actual);
    }

    @Test
    public void test2() {
        int[] A = {4, 1, 3};
        int expected = 0;

        int actual = solution(A);

        assertEquals(expected, actual);
    }

}
