package MissingInteger;


/*
Write a function:
class Solution { public int solution(int[] A); }
that, given a non-empty zero-indexed array A of N integers, returns the minimal positive integer that does not occur in A.
For example, given:
  A[0] = 1
  A[1] = 3
  A[2] = 6
  A[3] = 4
  A[4] = 1
  A[5] = 2
the function should return 5.
Assume that:
N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
Complexity:
expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.

*/

// you can also use imports, for example:
// import java.util.*;

// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

import org.junit.Test;
import static org.junit.Assert.*;

public class Solution {
    public int solution(int[] A) {
        // write your code in Java SE 8
        final int N = A.length;
        boolean[] seen = new boolean[N+1]; // [0] ignored
        for(int i= 0; i < N; ++i) {
            if(A[i] > 0 && A[i] <= N) {
                seen[A[i]] = true;
            }
        }

        for(int i = 1; i < N+1; ++i) {
            if(seen[i] == false) return i;
        }

        return N+1;
    }

    //-----------------------test------------------------------------

    @Test
    public void test1() {
        int[] A = {1, 3, 6, 4, 1, 2};
        int expected = 5;

        int actual = solution(A);

        assertEquals(expected, actual);
    }

    @Test
    public void test2() {
        int[] A = {-2, 29, -9, 0};
        int expected = 1;

        int actual = solution(A);

        assertEquals(expected, actual);
    }
}
