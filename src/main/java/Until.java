import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Until {
    // -----------------------helpers--------------------------------

    public static Random rdm = new Random();
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    // [from, to)
    public static int random(int from, int to) {
        return rdm.nextInt(to - from) + from;
    }

    // A[0]...A[n-1]
    public static int random(int[] A) {
        int index = random(0, A.length);
        return A[index];
    }
}
